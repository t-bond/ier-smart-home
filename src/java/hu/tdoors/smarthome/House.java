package hu.tdoors.smarthome;

import jason.asSyntax.Literal;
import jason.asSyntax.Structure;
import jason.environment.Environment;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class House extends Environment implements Controllable {

    private Thread guiThread;

    static Logger logger = Logger.getLogger(House.class.getName());

    private static final String AG_ALARM = "alarm";
    private static final String AG_LIGHT = "light";
    private static final String AG_MOTION_SENSOR = "motionSensor";
    private static final String AG_HVAC = "hvac";
    private static final String AG_VACUUM_CLEANER = "vacuumCleaner";

    private static final Literal PC_ALARM_ON = Literal.parseLiteral("armed(alarm)");
    private static final Literal PC_ALARM_OFF = Literal.parseLiteral("disarmed(alarm)");

    private static final Literal PC_MOTION = Literal.parseLiteral("motion(1)");
    private static final Literal PC_NOMOTION = Literal.parseLiteral("motion(0)");

    private static final Literal PC_UNI_DAY = Literal.parseLiteral("day(_)");

    private static final Literal PC_UNI_ENV_TEMP = Literal.parseLiteral("temperature(env,_)");
    private static final Literal PC_UNI_TAR_TEMP = Literal.parseLiteral("temperature(tar,_)");

    private static final String AC_OFF = "off";
    private static final String AC_ON = "on";

    private static final String AC_HVAC_COOLING = "cooling";
    private static final String AC_HVAC_HEATING = "heating";
    private static final String AC_TEMP_CHANGE = "temperature";
    private static final String AC_TEMP_ENVIRONMENT = "env";
    private static final String AC_TEMP_TARGET = "tar";

    private static final String AC_COLOR = "color";
    private static final String AC_COLOR_BLACK = "black";
    private static final String AC_COLOR_YELLOW = "yellow";
    private static final String AC_COLOR_RED = "red";

    private static final String AC_WAITING = "waiting";
    private static final String AC_PROGRESS = "progress";

    private final List<Observer> observers = new ArrayList<>();
    private final List<Structure> pendingActions = new ArrayList<>();

    @Override
    public void init(String[] args) {
        guiThread = new Thread(new GUIRunnable(this, args));
        guiThread.start();
        super.init(args);
    }

    @Override
    public boolean executeAction(String agName, Structure action) {
        //logger.info(agName+" doing: "+ action);

        if(observers.isEmpty())
            pendingActions.add(action);
        else
            handleAction(action);

        return true; // the action was executed with success
    }

    private void handleAction(Structure action) {
        //logger.info(action.getFunctor() + " " + action.getTerm(0).toString());

        if(AC_TEMP_CHANGE.equalsIgnoreCase(action.getFunctor())) {
            String context = action.getTerm(0).toString();
            int newValue = Integer.parseInt(action.getTerm(1).toString());

            if(AC_TEMP_TARGET.equalsIgnoreCase(context))
                observers.forEach(o -> o.onTarTempChange(newValue));
            else
            if(AC_TEMP_ENVIRONMENT.equalsIgnoreCase(context))
                observers.forEach(o -> o.onEnvTempChange(newValue));
        }else

        if(AC_PROGRESS.equalsIgnoreCase(action.getFunctor()))
            observers.forEach(o -> o.onVacuumProgress(Integer.parseInt(action.getTerm(0).toString())));
        else

        if(AC_OFF.equalsIgnoreCase(action.getFunctor())) {
            String ag = action.getTerm(0).toString();

            if(AG_HVAC.equalsIgnoreCase(ag))
                observers.forEach(o -> o.onHVACStatusChange(HVACStatus.IDLE));
            else
            if(AG_ALARM.equalsIgnoreCase(ag))
                observers.forEach(o -> o.onAlarmPowerChange(false));
            else
            if(AG_LIGHT.equalsIgnoreCase(ag))
                observers.forEach(o -> o.onLightPowerChange(false));
            else
            if(AG_MOTION_SENSOR.equalsIgnoreCase(ag))
                observers.forEach(o -> o.onMotionChange(false));
            else
            if(AG_VACUUM_CLEANER.equalsIgnoreCase(ag))
                observers.forEach(o -> o.onVacuumStatusChanged(VacuumStatus.STANDBY));
        }else

        if(AC_ON.equalsIgnoreCase(action.getFunctor())) {
            String ag = action.getTerm(0).toString();

            if(AG_ALARM.equalsIgnoreCase(ag))
                observers.forEach(o -> o.onAlarmPowerChange(true));
            else
            if(AG_LIGHT.equalsIgnoreCase(ag))
                observers.forEach(o -> o.onLightPowerChange(true));
            else
            if(AG_MOTION_SENSOR.equalsIgnoreCase(ag))
                observers.forEach(o -> o.onMotionChange(true));
            else
            if(AG_VACUUM_CLEANER.equalsIgnoreCase(ag))
                observers.forEach(o -> o.onVacuumStatusChanged(VacuumStatus.ON));
        }else



        if(AC_HVAC_COOLING.equalsIgnoreCase(action.getFunctor()))
            observers.forEach(o -> o.onHVACStatusChange(HVACStatus.COOLING));
        else
        if(AC_HVAC_HEATING.equalsIgnoreCase(action.getFunctor()))
            observers.forEach(o -> o.onHVACStatusChange(HVACStatus.HEATING));
        else


        if(AC_COLOR.equalsIgnoreCase(action.getFunctor())) {
            String ag = action.getTerm(0).toString();

            if(AG_LIGHT.equalsIgnoreCase(ag)) {
                String color = action.getTerm(1).toString();

                if(AC_COLOR_BLACK.equalsIgnoreCase(color))
                    observers.forEach(o -> o.onLightColorChange(Color.BLACK));
                else
                if(AC_COLOR_YELLOW.equalsIgnoreCase(color))
                    observers.forEach(o -> o.onLightColorChange(Color.YELLOW));
                else
                if(AC_COLOR_RED.equalsIgnoreCase(color))
                    observers.forEach(o -> o.onLightColorChange(Color.RED));
            }
        }else

        if(AC_WAITING.equalsIgnoreCase(action.getFunctor()))
            observers.forEach(o -> o.onVacuumStatusChanged(VacuumStatus.WAITING));
    }

    /** Called before the end of MAS execution */
    @Override
    public void stop() {
        super.stop();
        try {
            guiThread.join();
        } catch (InterruptedException ignored) {}
    }

    @Override
    public void changeAlarm(boolean isArmed) {
        removePercept(AG_ALARM, PC_ALARM_ON);
        removePercept(AG_ALARM, PC_ALARM_OFF);

        if(isArmed)
            addPercept(AG_ALARM, PC_ALARM_ON);
        else
            addPercept(AG_ALARM, PC_ALARM_OFF);
    }

    @Override
    public void onMotionChange(boolean hasMotion) {
        removePercept(AG_MOTION_SENSOR, PC_MOTION);
        removePercept(AG_MOTION_SENSOR, PC_NOMOTION);

        if(hasMotion)
            addPercept(AG_MOTION_SENSOR, PC_MOTION);
        else
            addPercept(AG_MOTION_SENSOR, PC_NOMOTION);
        informAgsEnvironmentChanged(AG_MOTION_SENSOR);
    }

    @Override
    public void changeHVAC (int target, int current) {
        removePerceptsByUnif(AG_HVAC, PC_UNI_ENV_TEMP);
        removePerceptsByUnif(AG_HVAC, PC_UNI_TAR_TEMP);

        addPercept(AG_HVAC, Literal.parseLiteral("temperature(env," + current + ")"));
        addPercept(AG_HVAC, Literal.parseLiteral("temperature(tar," + target + ")"));
        informAgsEnvironmentChanged(AG_HVAC);
    }

    @Override
    public void changeDay(int day) {
        removePerceptsByUnif(AG_VACUUM_CLEANER, PC_UNI_DAY);

        addPercept(AG_VACUUM_CLEANER, Literal.parseLiteral("day(" + day + ")"));
        informAgsEnvironmentChanged(AG_VACUUM_CLEANER);
    }

    @Override
    public void attachObserver(Observer observer) {
        observers.add(observer);

        for (Structure pendingAction : pendingActions)
            handleAction(pendingAction);
        pendingActions.clear();
    }

    private static class GUIRunnable implements Runnable {
        private final String[] args;
        private final Controllable c;

        GUIRunnable(Controllable c, String[] args) {
            this.c = c;
            this.args = args;
        }

        @Override
        public void run() {
            Dashboard.main(args, c);
        }
    }
}
