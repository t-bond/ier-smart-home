// Agent motionSensor in project smartHome.mas2j

/* Initial beliefs and rules */

/* Initial goals */

/* Plans */

+motion(1) : true <- on(motionSensor); .send(lighting, tell, motion(motionSensor)); .send(vacuumCleaner, tell, motion(motionSensor)).
+motion(0) : true <- off(motionSensor); .send(lighting, untell, motion(motionSensor)); .send(vacuumCleaner, untell, motion(motionSensor)).
