progress(0).
nothoovering.

+day(D) : D mod 5 == 0 & armed(alarm) & not motion(motionSensor) & nothoovering <- -nothoovering; +hoovering; on(vacuumCleaner); !clean.
+day(D) : D mod 5 == 0 & disarmed(alarm) | motion(motionSensor) & nothoovering <- +disturbed; waiting(vacuumCleaner).
+motion(motionSensor) : hoovering & progress(P) & P \== 100 <- +disturbed; waiting(vacuumCleaner); !stopHoovering.
+disarmed(alarm) : hoovering & progress(P) & P \== 100 <- +disturbed; waiting(vacuumCleaner); !stopHoovering.
-motion(motionSensor) : disturbed & armed(alarm) <- -disturbed; -nothovering; +hoovering; on(vacuumCleaner); !clean.
-disarmed(alarm) : disturbed & not motion(motionSensor) <- -disturbed; -nothovering; +hoovering; on(vacuumCleaner); !clean.

+!clean : not disturbed & progress(T) & T < 100 & hoovering <- Value=T+1; .wait(100); -+progress(Value); progress(Value); !clean.
+!clean : progress(T) & T == 100 & hoovering <- off(vacuumCleaner); progress(T); -progress(T); +progress(0); !stopHoovering.
+!clean : disturbed <- true.

+!stopHoovering : true <- -hoovering; +nothoovering.


