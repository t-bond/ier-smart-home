// Agent lighting in project smartHome.mas2j

off(light).
color(black).

+motion(motionSensor): armed(alarm) <- !on; !color(red).
-motion(motionSensor): disarmed(alarm) <- !off.

+motion(motionSensor): disarmed(alarm) <- !on; !color(yellow).

+disarmed(alarm): not motion(motionSensor) <- !off.
+disarmed(alarm): motion(motionSensor) <- !color(yellow).
+armed(alarm): motion(motionSensor) <- !color(red).

+!on: off(light) <- -off(light); +on(light); on(light).
+!off: on(light) <- !color(black); +off(light); -on(light); off(light).

+!off: off(light) <- true.
+!on: on(light) <- true.

+!color(black) : on(light) <- +color(black); -color(yellow); -color(red); color(light, black).
+!color(yellow) : on(light) <- -color(black); +color(yellow); -color(red); color(light, yellow).
+!color(red) : on(light) <- -color(black); -color(yellow); +color(red); color(light, red).
