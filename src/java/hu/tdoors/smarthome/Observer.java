package hu.tdoors.smarthome;

import javafx.scene.paint.Color;

public interface Observer {
    void onLightPowerChange(boolean isOn);
    void onLightColorChange(Color color);

    void onEnvTempChange(int temp);
    void onTarTempChange(int temp);
    void onHVACStatusChange(HVACStatus status);

    void onAlarmPowerChange(boolean isOn);

    void onMotionChange(boolean hasMotion);

    void onVacuumStatusChanged(VacuumStatus vacuumStatus);
    void onVacuumProgress(int progress);
}
