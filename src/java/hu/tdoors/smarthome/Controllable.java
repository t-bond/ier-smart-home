package hu.tdoors.smarthome;

public interface Controllable {
    void changeAlarm(boolean isArmed);
    void onMotionChange(boolean hasMotion);
    void changeHVAC(int target, int current);
    void attachObserver(Observer observer);
    void changeDay(int day);
}
