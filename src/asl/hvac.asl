// Agent hvac in project smartHome.mas2j

current_temp(22).
current_target(15).

!examine(task).

+disarmed(alarm) : true <- -+current_target(22); temperature(tar, 22); !examine(task).
+armed(alarm) : true <- -+current_target(15); temperature(tar, 15); !examine(task).

+temperature(env, C) : true <- -+current_temp(C); -temperature(env, C); !examine(task).
+temperature(tar, T) : disarmed(alarm) <- -+current_target(T); -temperature(tar, T); temperature(tar, T); !examine(task).

+!examine(task) : current_temp(C) & current_target(T) & C < T <- !function(heating).
+!examine(task) : current_temp(C) & current_target(T) & C > T <- !function(cooling).
+!examine(task) : current_temp(C) & current_target(T) & C == T <- !function(standby).

+!function(cooling) : not function(cooling) <- -function(heating); -function(standby); +function(cooling); cooling(hvac); !cooldown.
+!function(heating) : not function(heating) <- -function(cooling); -function(standby); +function(heating); heating(hvac); !warmup.
+!function(standby) : not function(standby) <- -function(heating); -function(cooling); +function(standby); off(hvac).

+!cooldown : current_temp(E) & current_target(T) & E > T <- Value=E-1; .wait(1000); -+current_temp(Value); temperature(env, Value); !cooldown.
+!cooldown : current_temp(E) & current_target(T) & E == T <- !function(standby).

+!warmup : current_temp(E) & current_target(T) & E < T <- Value=E+1; .wait(1000); -+current_temp(Value); temperature(env, Value); !warmup.
+!warmup : current_temp(E) & current_target(T) & E == T <- !function(standby).

// For alternative paths
+!function(cooling) : function(cooling) <- true.
+!function(heating) : function(heating) <- true.
+!function(standby) : function(standby) <- true.

+!cooldown : current_temp(E) & current_target(T) & E < T <- true.
+!warmup : current_temp(E) & current_target(T) & E > T <- true.