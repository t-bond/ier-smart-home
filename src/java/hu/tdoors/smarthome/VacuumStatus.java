package hu.tdoors.smarthome;

public enum VacuumStatus {
    STANDBY,
    WAITING,
    ON
}
