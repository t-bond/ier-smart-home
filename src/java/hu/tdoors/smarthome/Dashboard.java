package hu.tdoors.smarthome;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Dashboard extends Application {

    private static Controllable controller;

    public static void main(String[] args, Controllable controller) {
        Dashboard.controller = controller;
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/gui/Dashboard.fxml"));
        Parent dashboardController = loader.load();
        Controller controller = loader.getController();
        controller.attachController(Dashboard.controller);
        //Creating a scene object
        primaryStage.setTitle("tDoors - Smart Home");
        Scene s = new Scene(dashboardController, 800, 800);
        primaryStage.setScene(s);
        primaryStage.show();
    }
}
