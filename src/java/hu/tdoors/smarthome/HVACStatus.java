package hu.tdoors.smarthome;

public enum HVACStatus {
    IDLE,
    HEATING,
    COOLING
}
