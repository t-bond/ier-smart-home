// Agent alarm in project smartHome.mas2j

/* Initial beliefs and rules */

/* Initial goals */
!arm.
/* Plans */

+armed(alarm) : true <- !arm.
+disarmed(alarm) : true <- !disarm.

+!arm: true <- on(alarm); .send(hvac, tell, armed(alarm)); .send(hvac, untell, disarmed(alarm)); .send(lighting, tell, armed(alarm)); .send(lighting, untell, disarmed(alarm)); .send(vacuumCleaner,tell,armed(alarm)); .send(vacuumCleaner,untell,disarmed(alarm)).
+!disarm: true <- off(alarm); -armed(alarm); .send(hvac, tell, disarmed(alarm)); .send(hvac, untell, armed(alarm)); .send(lighting, tell, disarmed(alarm)); .send(lighting, untell, armed(alarm)); .send(vacuumCleaner,untell,armed(alarm)); .send(vacuumCleaner,tell,disarmed(alarm)).
