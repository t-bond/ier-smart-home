package hu.tdoors.smarthome;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextFormatter;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;

public class Controller implements Observer  {

    private final List<Controllable> controllers = new ArrayList<>();

    @FXML
    Text currTarTemp, currEnvTemp, lightPower, hvacStatusText, alarmStatusText, motionStatusText, vcmclStatusText, vacuumStatus;
    @FXML
    Button alarmAction, motionAction, tempButton;
    @FXML
    Spinner<Integer> targetTemp, currentTemp, currentDay;
    @FXML
    Circle colorAlarm, colorHVACStat, colorVCMCLStat, colorMotion;
    @FXML
    Rectangle colorLight;
    @FXML
    ProgressBar cleaningProgress;

    private boolean isArmed = true,
                    hasMotion = false;

    private final UnaryOperator<TextFormatter.Change> filterWithNegative = t -> {
        String newText = t.getControlNewText();
        if (newText.matches("-?[0-9]*"))
            return t;
        return null;
    }, filter = t -> {
        String newText = t.getControlNewText();
        if (newText.matches("[0-9]*"))
            return t;
        return null;
    };

    @SuppressWarnings("unused") // Used by JavaFX
    @FXML
    public void initialize()
    {
        targetTemp.increment(22);
        currentTemp.increment(22);
        targetTemp.getEditor().setTextFormatter(new TextFormatter<>(filterWithNegative));
        currentTemp.getEditor().setTextFormatter(new TextFormatter<>(filterWithNegative));
        currentDay.getEditor().setTextFormatter(new TextFormatter<>(filter));
        currentDay.valueProperty().addListener((obs, oldValue, newValue) -> controllers.forEach(c -> c.changeDay(newValue)));
    }

    public void onAlarmStatusChange()
    {
        isArmed = !isArmed;
        alarmAction.setText(isArmed ? "Disarm" : "Arm");
        controllers.forEach(c -> c.changeAlarm(isArmed));
    }

    public void setHVAC()
    {
        controllers.forEach(c -> c.changeHVAC(targetTemp.getValue(), currentTemp.getValue()));
    }

    public void onMotionChange()
    {
        hasMotion = !hasMotion;
        motionAction.setText(hasMotion ? "Stop moving" : "Move");
        controllers.forEach(c -> c.onMotionChange(hasMotion));
    }

    public void attachController(Controllable controller)
    {
        if(controller == null)
            return;

        controllers.add(controller);
        controller.attachObserver(this);
    }

    @Override
    public void onLightPowerChange(boolean isOn) {
        Platform.runLater(() -> lightPower.setText(isOn ? "ON" : "OFF"));
    }

    @Override
    public void onLightColorChange(Color color) {
        Platform.runLater(() -> colorLight.setFill(color));
    }

    @Override
    public void onEnvTempChange(int temp) {
        Platform.runLater(() -> currEnvTemp.setText(String.valueOf(temp)));
    }

    @Override
    public void onTarTempChange(int temp) {
        Platform.runLater(() -> currTarTemp.setText(String.valueOf(temp)));
    }

    @Override
    public void onHVACStatusChange(HVACStatus status) {
        Color stateColor;
        String stateText;
        switch (status) {
            case COOLING:
                stateColor = Color.BLUE;
                stateText = "COOLING";
                break;
            case HEATING:
                stateColor = Color.RED;
                stateText = "HEATING";
                break;
            default:
            case IDLE:
                stateColor = Color.YELLOW;
                stateText = "IDLE";
        }

        Platform.runLater(() -> {
            hvacStatusText.setText(stateText);
            colorHVACStat.setFill(stateColor);
        });
    }

    @Override
    public void onAlarmPowerChange(boolean isOn) {
        Platform.runLater(() -> {
            colorAlarm.setFill(
                    isOn ? Color.RED : Color.GREEN
            );
            alarmStatusText.setText(isOn ? "Armed" : "Disarmed");
        });
    }

    @Override
    public void onMotionChange(boolean hasMotion) {
        Platform.runLater(() -> {
            colorMotion.setFill(
                    hasMotion ? Color.RED : Color.GREEN
            );
            motionStatusText.setText(hasMotion ? "Detected" : "Undetected");
        });
    }

    @Override
    public void onVacuumStatusChanged(VacuumStatus cleaningStatus) {
        Color stateColor;
        String stateText;
        switch (cleaningStatus) {
            case ON:
                stateColor = Color.GREEN;
                stateText = "Cleaning...";
                break;
            case WAITING:
                stateColor = Color.YELLOW;
                stateText = "Waiting...";
                break;
            default:
            case STANDBY:
                stateColor = Color.BLACK;
                stateText = "Off";
        }

        Platform.runLater(() -> {
            vcmclStatusText.setText(stateText);
            colorVCMCLStat.setFill(stateColor);
        });
    }

    public void onVacuumProgress(int progress) {
        double percent = progress / 100.d;
        Platform.runLater(() -> {
            vacuumStatus.setText(String.format("%.0f%%", Math.floor(percent * 100)));
            cleaningProgress.setProgress(percent);
        });
    }
}
